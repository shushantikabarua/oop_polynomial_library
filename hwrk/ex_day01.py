import math

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def get_distance(self):
        return math.sqrt(self.x**2 + self.y**2)

class Rectangle:
    def __init__(self, width, height, point):
        self.w = width
        self.h = height
        self.p = point
        self.x = point.x
        self.y = point.y

    def get_center(self):
        self.center = Point()
        center.x = self.x + self.w/2
        center.y = self.y + self.h/2
        return self.center

    def change_width(self, width):
        self.w += width

    def change_height(self, height):
        self.h += height

    def __str__(self):
        center = get_center(self)
        return f"This is a {self.w}x{self.h} rectangle with the center at {center}"
