import math

class Point:
    """Represents a point in 2-D space."""

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'Point({self.x}, {self.y})'

    def __str__(self):
        return f'({self.x}, {self.y})'

    def dist_from_origin(self):
        return (self.x ** 2 + self.y ** 2) ** 0.5

    def distance(self, p):
        return math.sqrt(((p.x - self.x) ** 2 ) + ((p.y - self.y) ** 2))

class LineSegment:
    """Represents a line segment in 2-D space"""

    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2

    def __repr__(self):
        return f'LineSegment({self.p1}, {self.p2})'

    def __str__(self):
        return f'({self.p1}, {self.p2})'

    def slope(self):
        """
         >>> p1 = Point(0, 1)
         >>> p2 = Point(1, 2)
         >>> li = LineSegment(p1, p2)
         >>> li.slope()
         1.0
        """
        return round((self.p2.y - self.p1.y) / (self.p2.x - self.p1.x), 3)

    def length(self):
        """
         >>> p1 = Point(0, 2)
         >>> p2 = Point(4, 8)
         >>> li = LineSegment(p1, p2)
         >>> li.length()
         7.211
        """
        return round((self.p1.distance(self.p2)), 3)

    def y_intercept(self):
        """
         >>> p1 = Point(0, 2)
         >>> p2 = Point(4, 8)
         >>> li = LineSegment(p1, p2)
         >>> li.y_intercept()
         2.0
        """
        return round(self.p1.y - (self.slope() * self.p1.x), 3)

    def x_intercept(self):
        pass

    def midpoint(self):
        """
         >>> p1 = Point(0, 2)
         >>> p2 = Point(4, 8)
         >>> li = LineSegment(p1, p2)
         >>> li.midpoint()
         Point(2.0, 5.0)
        """
        return Point(round((self.p1.x + self.p2.x) / 2, 3), round((self.p1.y + self.p2.y) / 2, 3))

class Rectangle:
    """Represents a rectangle in 2-D space"""

    def __init__(self, origin, width, height):
        self.o = origin
        self.w = width
        self.h = height

    def __repr__(self):
        return f'Rectangle({self.o}, {self.w}, {self.h})'

    def __str__(self):
        return f'({self.o}, {self.w}, {self.h}'

    def perimeter(self):
        """
         >>> origin = Point(0, 10)
         >>> width = 5
         >>> height = 5
         >>> rec = Rectangle(origin, width, height)
         >>> rec.perimeter()
         20
        """
        return round((self.w * 2) + (self.h * 2), 3)

    def area(self):
        """
         >>> origin = Point(0, 10)
         >>> width = 5
         >>> height = 5
         >>> rec = Rectangle(origin, width, height)
         >>> rec.area()
         25
        """
        return round(self.w * self.h, 3)

    def corners(self):   #c1 - upper left; c2 - lower left; c3 - upper right; c4 - lower right
        """
         >>> origin = Point(0, 10)
         >>> width = 5
         >>> height = 5
         >>> rec = Rectangle(origin, width, height)
         >>> rec.corners()
         [Point(0, 10), Point(0, 5), Point(5, 10), Point(5, 5)]
        """
        c1 = self.o
        c2 = Point(self.o.x, self.o.y - self.h)
        c3 = Point(self.o.x + self.w, self.o.y)
        c4 = Point(self.o.x + self.w, self.o.y - self.h)
        return [c1, c2, c3, c4]

    def sides(self):   #s1 - ul to ll; s2 - ul to ur; s3 - ur to lr; s4 - ll to lr
        """
         >>> origin = Point(0, 10)
         >>> width = 5
         >>> height = 5
         >>> rec = Rectangle(origin, width, height)
         >>> rec.sides()
         [LineSegment((0, 10), (0, 5)), LineSegment((0, 10), (5, 10)), LineSegment((5, 10), (5, 5)), LineSegment((0, 5), (5, 5))]
        """
        c = self.corners()
        s1 = LineSegment(c[0], c[1])
        s2 = LineSegment(c[0], c[2])
        s3 = LineSegment(c[2], c[3])
        s4 = LineSegment(c[1], c[3])
        return [s1, s2, s3, s4]

    def diagonals(self): #d1 - ul to lr; d2 - ll to ur
        """
         >>> origin = Point(0, 10)
         >>> width = 5
         >>> height = 5
         >>> rec = Rectangle(origin, width, height)
         >>> rec.diagonals()
         [LineSegment((0, 10), (5, 5)), LineSegment((0, 5), (5, 10))]
        """
        c = self.corners()
        d1 = LineSegment(c[0], c[3])
        d2 = LineSegment(c[1], c[2])
        return [d1, d2]

class Circle:
    """Represents a circle in 2-D space"""

    def __init__(self, origin, radius):
        self.o = origin
        self.r = radius

    def __repr__(self):
        return f'Circle({self.o}, {self.r})'

    def __str__(self):
        return f'({self.o}, {self.r}'

    def diameter(self):
        """
         >>> origin = Point(0, 10)
         >>> radius = 5
         >>> cir = Circle(origin, radius)
         >>> cir.diameter()
         10
        """
        return 2 * self.r
    
    def circumference(self):
        """
         >>> origin = Point(0, 10)
         >>> radius = 5
         >>> cir = Circle(origin, radius)
         >>> cir.circumference()
         31.4
        """
        return self.diameter() * math.pi

    def area(self):
        """
         >>> origin = Point(0, 10)
         >>> radius = 5
         >>> cir = Circle(origin, radius)
         >>> cir.circumference()
         78.5
        """
        return (self.r ** 2) * math.pi
    
if __name__ == "__main__":
    import doctest
    doctest.testmod()
