# OOP | Polynomial Library

A polynomial library created using object-oriented programming in Python.

## Prerequisites

Python 3.6 or later

## Folder Structure

`prac/` contains the practice done in class and at home

`hwrk/` contains the assigned homework

`lib/` contains the actual library

## Authors

Shushantika Barua

Stefan-Ionut Barbu